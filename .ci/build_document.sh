set -e
echo "Building document"
make pdf
mkdir public
mv build/2023_Game_Theory_in_Flow_Problems.pdf public
mv build/2023_Game_Theory_in_Flow_Problems.log public
cd public/
if ! command -v tree &> /dev/null
then
  echo "No tree utility found, skipping making tree"
else
  tree -H '.' -I "index.html" -D --charset utf-8 -T "Optimization and Game Theory in Flow Problems" > index.html
fi
