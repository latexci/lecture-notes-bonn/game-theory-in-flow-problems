\lecture[Motivation: Traffic flows, Braess's paradox. Max Flows over time. Temporally repeated flows and their values. Ford-Fulkerson algorithm.]{Mo 9 October 2023}{Max flows over time}
\section{Introduction}

\begin{question}
  Which problems can be modelled as flow problems?
\end{question}

\begin{example}
  One interesting application are \emph{Traffic Flows}.
  \begin{itemize}
    \item For example, a central authority might coordinate how people evacuate in case
      of a nearing flood.
      This is an example of a \vocab{flow over time} 
    \item In a typical real-life situation, however, there is no such central authority
      that dictates peoples' routes.
      Thus, the setting resembles much rather a \emph{game theoretical problem},
      since each participant will individually decide which route to take,
      potentially leading to solutions that are not globally optimal.

      This can lead to unexpected outcomes:
  \end{itemize}
\end{example}

\begin{example}[Breass's Paradox]
  Consider the following traffic network, where we want to route a total flow of $1$:
  \missingfigure{tikz}
  Here, an edge of cost $x$ will have cost equal to the amount of flow routed along it.

  The optimum solution (in terms of maximum travel time)
  is of course to route flow $\frac{1}{2}$ along each of the paths.

  It can be shown that this is also the solution that people will actually choose,
  since it is always better to use the currently less-congested path,
  leading to an equal spread.

  However, now considering adding an additional
  \todo{finish remark}.
\end{example}


\section{Max Flows over time}

\begin{definition}[Flow over time]
  Consider a graph $G = (V,E)$ with \vocab{transit times} $\set{ τ_e } _{e\in E}$
  and capacities $\set{ u_e } _{e\in E}$.

  A \vocab{flow over time} $f$ time horizon $T$ is a
  Lebesgue integrable function $f_e\colon [0, T) \to \mathbb{R}_{\geq 0}$
  for $e\in E$ such that $f_{e}(\theta) = 0$ for $\theta \geq T - τ_e$.
\end{definition}

\begin{oral}
  The assumption on the Lebesgue integrability is just here for technical reasons,
  we will not deal much with it.
  In practice, most functions that we will encounter are piecewise constant.
\end{oral}

\begin{remark}+
  We can interpret this definition as edges signaling pipes,
  and the flow $f_e$ denoting the inflow rate to edge $e$ over a certain time.
  Then, it takes $τ_e$ time for the flow to traverse the pipes.
  Thus, $f_e(\theta - τ_e)$ denotes the excess flow of edge $e$ at time $\theta$.
\end{remark}

\setcounter{toplevel}{2}
\begin{definition}
  Let $f$ be a flow over time with horizon $T$.
  \begin{enumerate}[a)]
    \item The \vocab{capacity constraint} is that $f_e(\theta) \leq u_e$ for all $e,\theta$.
    \item The \vocab{excess for a vertex} $v\in V$ at $\theta$ is defined as
      \[
        \ex_f(v, \theta) \coloneq
        \sum_{e\in δ^-(v)} \int_0^{\zeta - τ_e} f_e(\zeta) d \zeta
        -
        \sum_{e\in δ^+(v)} \int_0^{\zeta} f_e(\zeta) d \zeta
      \]
    \item We say that $f$ has \vocab{weak flow conservation} if
      \begin{IEEEeqnarray*}{rCLl}
        \ex_f(v, \theta) &\geq& 0 & \forall v \in V \setminus \set{ s } \forall \theta \in [0,T)
        \\
        \ex_f(v, T) = 0 & \forall v \in V \setminus \set{ s,t } 
      \end{IEEEeqnarray*}
    \item We say that $f$ has \vocab{strict flow conservation} if
      \[
        \ex_f(v, \theta) = 0 \forall v\in V \\ \set{ s , t}  \forall \theta \in  T
      \]
    \item The \vocab{value of f} is $\abs{f} \coloneqq \ex_f(t,T)$.
    \item A flow is called \vocab{feasible} if it satisfies the capacity constraints 
      and has weak flow conservation.
  \end{enumerate}
\end{definition}


Consider a graph $G = (V,E)$ with capacities $c\colon E \to  \mathbb{R}_{\geq 0}$,
transit times $τ\colon E\to \mathbb{R}_{\geq 0}$,
a time horizon $T$ and source/sink vertices $s$, $t$.

The \textsc{Maximum Flow over Time Problem} asks for a feasible $s$-$t$-flow
over time with time horizon $T$ and maximum value of $\abs{f}$.

\begin{definition}[Temporally repeated flow with time horizon $T$]
  Let $x$ be a static\footnotemark $s$-$t$ (feasible) flow and consider a flow decomposition
  $(X_p)_{p \in \mathcal{P} \cup \mathcal{C}}$
  with $\abs{\mathcal{P} \cup \mathcal{C}} \leq \abs{E}$.

  Now, define a flow over time by
  \[
    f_e(\theta) \coloneqq \sum_{P \in P_e(\theta) } x_p \qquad
    \forall  e = (v,w) \in E \; \theta \in [0, T)
    ,
  \]
  where
  \[
    P_e(\theta) \coloneqq \set{
      p\in \mathcal{P} \suchthat
      p \in \mathcal{P},
      τ(P_{s,v)} \leq \theta,
      τ(P_{v, t}) \leq T - \theta
    }
    .
  \]
  Here, the transit times along a fixed path $P$ are defined canonically as
  \[
    τ(P_{v,w}) \coloneqq \sum_{e\in P_{v,w}} τ_e
    .
  \]

  The resulting flow is called the \vocab{temporally repeated flow with time horizon $T$}
  associated to the static flow $x$.
\end{definition}
\footnotetext{Here, \enquote{static} means just \enquote{not temporal}}

\begin{observation}
  \label{obs:temporally-repeated-flow-construction}
  The Temporally repeated flow can be obtained as follows:
  For each $P \in \mathcal{P}$, we send $x_p$ into $P$
  from $s$ during $[0, T - τ(P))$ and then pass the flow along $P$.

  Note that the temporally repeated flow even fulfills strict flow conservation.
\end{observation}


\setcounter{toplevel}{6}
\begin{lemma}
  Let $x$ be a static, feasible $s$-$t$ flow with
  flow decomposition $(x_P)_{P \in \mathcal{P} \cup \mathcal{C}}$
  and $x_P = 0$ for all $P \in \mathcal{P}$ with $τ(P) > T$
  and for all cycles.
  Denote by $f$ the corresponding temporally repeated flow $f$.

  Then,
  \[
    \abs{f} = T \cdot \abs{x}  - \sum_{e\in E} τ_e \cdot x_e
    .
  \]
\end{lemma}

\begin{proof}
  Motivated by \autoref{obs:temporally-repeated-flow-construction},
  we deduce:
  \begin{IEEEeqnarray*}{rCl}
    \abs{f}
    &=& \sum_{P\in \mathcal{P}} (T - τ(P)) \cdot x_P \\
    &=& T \cdot \sum_{P\in \mathcal{P}} x_P - \sum_{P\in \mathcal{P}} τ(P) \cdot x_P \\
    &=& T \cdot \abs{x} - \sum_{e\in E} τ_e \underbrace{\sum_{\substack{P\in \mathcal{P} \\ e \in P} } x_p}_{= x_e}
  \end{IEEEeqnarray*}
\end{proof}

\begin{corollary}
  Let $x$ be a static, feasible $s$-$t$ flow and $(x_P)_{P\in \mathcal{P} \cup \mathcal{C}}$
  a flow decomposition.
  Then,
  \[
    \abs{f} \geq T \cdot \abs{x} - \sum_{e\in E}  τ_e x_e
    .
  \]
\end{corollary}

\begin{proof}
  Define a new flow by
  \[
    \tilde{x}_P \coloneqq \begin{cases}
      x_P & \text{if $P\in \mathcal{P}$ and $τ(P) \leq T$} \\
      0 & \text{otherwise}
    \end{cases}
  \]
  for all $P \in \mathcal{P}\cup \mathcal{C}$.
  Then, the associated temporally repeated flows satisfy
  \begin{IEEEeqnarray*}{rCl}
    \abs{f} &=& \abs{\tilde{f}} \\
            &=& T \abs{\tilde{x}} - \sum_{e \in  E} τ_e \tilde{x}_e \\
            &\geq & T \abs{x} - \sum_{e\in E} τ_e x_e
  \end{IEEEeqnarray*}
  Note that to check the last inequality,
  it suffices to see that cycles only contribute to the negative sum
  and do not change the flow value of $\tilde{x}$ compared to $x$.
  For paths $P$ with $τ(P) > T$, these contribute to $\abs{x} $
  but not to $\tilde{x}$, however, the effect cancels out by summing
  the $τ_e$ along this path in the negative summand.
\end{proof}

\begin{algorithm}[H]
  \SetKwInput{KwInput}{Input}
  \SetKwInput{KwOutput}{Output}
  \SetKw{KwGoTo}{go to}
  \SetKwProg{Fn}{Def}{:}{}
  \DontPrintSemicolon

  \caption{Ford-Furkerson Algorithm for maximum flows over time}
  \KwInput{$G = (V, E)$ with capacities $c$, transit times $τ$, time horizon $T$ and source/sink vertices $s$, $t$.}
  \KwOutput{A feasible temporally repeated flow}
  \;

  Compute a feasible static $s$-$t$-flow $x$ maximizing
  \[
    T \cdot \abs{x} - \sum_{e \in E} τ_e x_e 
  \]
  \;

  Compute a flow decomposition $(x_P)_{P\in \mathcal{P} \cup \mathcal{C}}$.
  \;

  Output corresponding temporally repeated flow.
  
\end{algorithm}


\begin{remark}
  To implement step 1, consider the auxiliary graph $G'$ obtained from $G$
  by adding an edge $(t,s)$ with infinite capacity.
  For each edge, set its cost to $τ_e$.

  Now, for each static $s$-$t$ flow $x$ in $G$ gives rise to a circulation
  in $G'$ by sending $\abs{x}$ along $(t,s)$, whose cost will be precisely
  \[
    \sum_{e\in E} x_e \cdot τ_e + \abs{x} \cdot (-T) 
    .
  \]
  Thus, solving the minimum cost flow problem in $G'$ yields a desired flow $f$.
\end{remark}
