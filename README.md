# Optimization and Game Theory in Flow Problems 

These are the lecture notes for the 'Optimization and Game Theory in Flow Problems', taught in winter rem 23/24 at the University of Bonn.

The [latest version][1] is availabe as a pdf download via GitLab runner.
You can also have a look at the generated [log files][2] or visit the
[gl pages][3] index directly.

[1]: https://latexci.gitlab.io/lecture-notes-bonn/game-theory-in-flow-problems/2023_Game_Theory_in_Flow_Problems.pdf
[2]: https://latexci.gitlab.io/lecture-notes-bonn/game-theory-in-flow-problems/2023_Game_Theory_in_Flow_Problems.log
[3]: https://latexci.gitlab.io/lecture-notes-bonn/game-theory-in-flow-problems/
